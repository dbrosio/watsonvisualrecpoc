const http = require('http');
require('dotenv').config();

const hostname = '127.0.0.1';
const port = 3002;

const fs = require('fs');
const VisualRecognitionV3 = require('ibm-watson/visual-recognition/v3');
const { IamAuthenticator } = require('ibm-watson/auth');

const visualRecognition = new VisualRecognitionV3({
  version: '2018-03-19',
  authenticator: new IamAuthenticator({
    apikey: process.env.VISUAL_RECOGNITION_APIKEY,
  }),
  url: process.env.VISUAL_RECOGNITION_URL,
});

const classifyParams = {
  imagesFile: fs.createReadStream('./Datasets/tests/6.jpg'),
  owners: ['me'],
  threshold: 0.6,
  classifierIds: 'empleados-blx_1941740838',
};


const createClassifierParams = {
  name: 'empleados-blx',
  negativeExamples: fs.createReadStream('./Datasets/randomFaces.zip'),
  positiveExamples: {
    jerrySeinfeld: fs.createReadStream('./Datasets/jerrySeinfeld.zip'),
    mindyKaling: fs.createReadStream('./Datasets/mindyKaling.zip'),
  }
};

const listClassifiersParams = {
  verbose: true,
};

const updateClassifierParams = {
  classifierId: 'empleados-blx_1941740838',
  negativeExamples: fs.createReadStream('./Datasets/randomFamosos.zip'),
  positiveExamples: {
    benAfflek: fs.createReadStream('./Datasets/benAfflek.zip'),
  },
};

const deleteClassifierParams = {
  classifierId: 'empleados-blx_1544221088',
};

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/html");
  res.end('<h2>Starting...<h2>');


  /*            CLASIFICAR            */
  // visualRecognition.classify(classifyParams)
  // .then(response => {
  //   const classifiedImages = response.result;
  //   console.log(JSON.stringify(classifiedImages, null, 2));
  // })
  // .catch(err => {
  //   console.log('error:', err);
  // });

  /*        CREAR CLASIFICADOR        */
  // visualRecognition.createClassifier(createClassifierParams)
  // .then(response => {
  //   const classifier = response.result;
  //   console.log(JSON.stringify(classifier, null, 2));
  // })
  // .catch(err => {
  //   console.log('error:', err);
  // });

  /*       LISTA CLASIFICADORES       */
  visualRecognition.listClassifiers(listClassifiersParams)
  .then(response => {
    const classifiers = response.result;
    console.log(JSON.stringify(classifiers, null, 2));
  })
  .catch(err => {
    console.log('error:', err);
  });

  /*       EDITAR CLASIFICADOR        */
  // visualRecognition.updateClassifier(updateClassifierParams)
  // .then(response => {
  //   const classifier = response.result;
  //   console.log(JSON.stringify(classifier, null, 2));
  // })
  // .catch(err => {
  //   console.log('error:', err);
  // });

  /*       ELIMINAR CLASIFICADOR        */
  // visualRecognition.deleteClassifier(deleteClassifierParams)
  // .then(result => {
  //   console.log(JSON.stringify(result, null, 2));
  // })
  // .catch(err => {
  //   console.log('error:', err);
  // });

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});